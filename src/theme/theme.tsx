import { extendTheme } from '@chakra-ui/react';

export const theme = extendTheme({
  components: {
    Button: {
      variants: {
        'person': {
          w: 'full',
          justifyContent: 'flex-start',
          paddingLeft: 0
        }
      }
    }
  },
  textStyles: {
    description: {
      fontSize: '12px',
      fontWeight: 'bold'
    },
    content: {
      fontSize: '16px',
      marginTop: '5px'
    }
  }
})
