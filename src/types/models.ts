import {dateString} from "./dates";

export type Person = {
    id: number,
    last_name: string,
    first_name: string,
    birth_date: dateString,
}

export type Adult = Person & {
    club_member: boolean,
    staff: boolean,
    household_size: number,
    no_kiga_fee: boolean,
    address: string | null,
    partner: string | null
}

export type AdultWithDetails = Adult & {
    address: {
        id: number,
        street: string,
        house_number: string,
        zip_code: string,
        city: string
    },
    partner: {
        id: number,
        last_name: string,
        first_name: string
    },
    children: [
        {
            id: number,
            last_name: string,
            first_name: string,
            // TODO Add proper type
            kinship: string,
            liable: boolean
        }
    ]
}

export type Child = Person & {
    birth_place: string,
    care_time: number,
    kita: boolean
}

export type ChildWithDetails = Child & {
    parents: [
        {
            id: number,
            last_name: string,
            first_name: string,
            kinship: string
        }
    ]
}
