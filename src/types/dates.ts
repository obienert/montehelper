type oneToNine = 1|2|3|4|5|6|7|8|9
type zeroToNine = 0|1|2|3|4|5|6|7|8|9
type zeroToEigth = 0|1|2|3|4|5|6|7|8

type YYYY = '${zeroToNine}${zeroToNine}${zeroToNine}${zeroToNine}'

type MM = `0${oneToNine}` | `1${0|1|2}`

type D31 = `${0}${oneToNine}` | `${1|2}${zeroToNine}` | `3${0|1}`
type D30 = `${0}${oneToNine}` | `${1|2}${zeroToNine}` | `30`
type D28 = `${0}${oneToNine}` | `${1|2}${zeroToEigth}`

type MMDD =
    | `01-${D31}`
    | `02-${D28}`
    | `03-${D31}`
    | `04-${D30}`
    | `05-${D31}`
    | `06-${D30}`
    | `07-${D31}`
    | `08-${D31}`
    | `09-${D30}`
    | `10-${D31}`
    | `11-${D30}`
    | `12-${D31}`

export type dateString = `${YYYY}-${MMDD}`
