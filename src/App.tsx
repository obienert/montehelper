import React, { useEffect, useState } from 'react';
import { Routes, Route } from "react-router-dom"
import Main from './pages/Main';
import Login from './pages/Login';
import useAuth  from './utils/APIHelper';
import RequireAuth from './components/atoms/RequireAuth';
import { Spinner } from '@chakra-ui/react';
import { APIContextProvider } from './utils/APIContextProvider';

const App = () => {
  const { getSession } = useAuth()
  const [loading, setLoading] = useState(true)

  useEffect(() => {
      getSession().then(() => setLoading(false));
  }, [])

  return (
    loading ? <Spinner /> :
    <Routes>
      <Route path="/" element={
        <RequireAuth>
          <APIContextProvider>
            <Main />
          </APIContextProvider>
        </RequireAuth>
      } />
      <Route path="/login" element={<Login />} />
    </Routes>
  );
}

export default App;
