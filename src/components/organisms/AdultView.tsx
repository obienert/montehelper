import React, { FC, useEffect, useState } from 'react';
import {Button, Checkbox, HStack, Text, VStack} from '@chakra-ui/react';
import useAPI from '../../utils/APIContextProvider';
import { useTranslation } from "react-i18next";
import { Adult, AdultWithDetails } from "../../types/models";
import DetailsFrame from "../atoms/DetailsFrame";
import FullName from "../atoms/FullName";

export type AdultViewProps = {
  adultID: number
  close: () => void
  showAdult: (adultId: number) => void
  showChild: (childId: number) => void
}

const AdultView: FC<AdultViewProps>  = ({adultID, close, showAdult, showChild}) => {
  const { t } = useTranslation()

  const { getAdult } = useAPI()
  const [adult, setAdult] = useState<AdultWithDetails>();
  const [partner, setPartner] = useState<Adult>();
  const [error, setError] = useState(false)

  useEffect(() => {
    const fetchData = async () => {
      const adult = await getAdult(adultID);
      const partner = await getAdult(adult?.partner?.id)

      setAdult(adult)
      setPartner(partner)
    }

    fetchData().catch(error => {
      console.log(error)
      setError(true)
    });
  }, [adultID, getAdult])

  return (
    <DetailsFrame close={close}>
      {error && <Text>Unexpected error occurred.</Text>}
      {(!adult && !error) && <Text>No adult with id {adultID} exists.</Text>}
      {adult &&
        <>
          <FullName person={adult}/>
          <Text textStyle='description' >{t('birthdate')}</Text>
          <Text textStyle='content' >{t('birthDateFormat', { date: new Date(adult.birth_date) })}</Text>
          <Checkbox defaultChecked={adult.club_member} isReadOnly>
            {t('clubMember')}
          </Checkbox>
          <Checkbox defaultChecked={adult.staff} isReadOnly>
            {t('staff')}
          </Checkbox>
          {adult.household_size && <>
            <Text textStyle='content' >{adult.household_size}</Text>
            <Text textStyle='description' >{t('householdSize')}</Text>
          </>}
          <Checkbox defaultChecked={adult.no_kiga_fee} isReadOnly>
            {t('noKigaFee')}
          </Checkbox>
          {partner && <>
            <Text textStyle='description' >{t('partner')}</Text>
            <Button onClick={() => showAdult(adult.partner.id)} variant={"person"}>
              {partner.first_name + " " + partner.last_name}
            </Button></>}
          {(Array.isArray(adult.children) && adult.children.length > 0) && <Text textStyle='description' >{t('children')}</Text>}
          {adult.children?.map(child => {
            if (child) {
              return (<Button onClick={() => showChild(child.id)} variant={"person"}>
                {child.first_name + " " + child.last_name}
              </Button>)
            }
            return null
          })}
        </>
      }
    </DetailsFrame>
  );
};

export default AdultView;
