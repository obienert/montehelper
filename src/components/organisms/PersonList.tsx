import React, { FC, useMemo } from 'react';
import { Box, Checkbox, Flex, Spinner } from '@chakra-ui/react';
import { Tabs, TabList, TabPanels, Tab, TabPanel } from '@chakra-ui/react'
import '../../styles/personList.css'
import PersonTable from '../molecules/PersonTable';
import useAPI from '../../utils/APIContextProvider';
import { Adult, Child } from '../../types/models';
import { useTranslation } from "react-i18next";
import { filterList } from "../../utils/Misc";
import { PersonType } from "../../pages/Main";

export type PersonListType = {
  showAdult: (id: number) => void
  showChild: (id: number) => void
  personID: number | undefined
  filter: string
  personType: PersonType
  setPersonType: (personType: PersonType) => void
}

const PersonList: FC<PersonListType> = ({
                                          showAdult,
                                          showChild,
                                          personID,
                                          filter,
                                          personType,
                                          setPersonType }) => {
  const { t } = useTranslation()

  const { adults, loadingAdults, children, loadingChildren } = useAPI()

  const columnsAdults = useMemo(() => [
    {
      Header: t('firstName'),
      accessor: 'first_name'
    },
    {
      Header: t('lastName'),
      accessor: 'last_name'
    },
    {
      Header: t('birthdate'),
      accessor: (row: Adult) => t('birthDateFormat', { date: new Date(row.birth_date) })
    },
    {
      Header: t('partner'),
      accessor: 'partner'
    }
  ], [t])

  const columnsChildren = useMemo(() => [
    {
      Header: t('firstName'),
      accessor: 'first_name'
    },
    {
      Header: t('lastName'),
      accessor: 'last_name'
    },
    {
      Header: t('birthdate'),
      accessor: (row: Child) => t('birthDateFormat', { date: new Date(row.birth_date) })
    },
    {
      Header: t('birthplace'),
      accessor: 'birth_place'
    },
    {
      Header: t('careTime'),
      accessor: 'care_time'
    },
    {
      Header: t('kita'),
      accessor: row => <Checkbox isChecked={row.kita} isReadOnly></Checkbox>
    }
  ], [t])

  const getFilteredAdults = () => {
    return filterList(adults, filter)
  }

  const getFilteredChildren = () => {
    return filterList(children, filter)
  }

  return (
    <Flex
      direction="column"
      flexGrow={3}
      h="full"
      p={5}
      alignItems="flex-start"
    >
      <Tabs w="full" h="full" index={personType} onChange={(index) => setPersonType(index) }>
        <TabList>
          <Tab>{t('adults')}</Tab>
          <Tab>{t('children')}</Tab>
        </TabList>

        <TabPanels>
          <TabPanel>
            <Box w="full" h="full">
              {loadingAdults ?
                  <Spinner className={"centered"}/> :
                  <PersonTable
                      persons={getFilteredAdults()}
                      showPerson={showAdult}
                      personID={personID}
                      /* TODO see https://stackoverflow.com/questions/72675589/why-does-react-table-column-not-accept-a-function-as-accessor*/
                      /*// @ts-ignore */
                      columns={columnsAdults}/>
              }
            </Box>
          </TabPanel>
          <TabPanel>
            <Box w="full" h="full">
              {loadingChildren ?
                  <Spinner className={"centered"}/> :
                  <PersonTable
                      persons={getFilteredChildren()}
                      showPerson={showChild}
                      personID={personID}
                      /* TODO see https://stackoverflow.com/questions/72675589/why-does-react-table-column-not-accept-a-function-as-accessor*/
                      /*// @ts-ignore */
                      columns={columnsChildren}/>
              }
            </Box>
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Flex>
  );
};

export default PersonList;
