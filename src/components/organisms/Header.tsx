import React, {FC} from 'react';
import { Box, Button, Flex, Icon, Input, Spacer } from '@chakra-ui/react';
import { FiLogOut } from 'react-icons/fi';
import useAuth from '../../utils/APIHelper';
import {useTranslation} from "react-i18next";
import LanguageSwitcher from "../atoms/LanguageSwitcher";

export type HeaderProps = {
  setFilter: (filter: string) => void
}

const Header: FC<HeaderProps> = ({setFilter}) => {
  const { t } = useTranslation()

  const { logout } = useAuth()

  return (
    <Box bg="gray.900" w="full" p={5} color="white">
      <Flex direction="row">
        <LanguageSwitcher />
        <Input
          paddingLeft={5}
          variant='unstyled'
          placeholder= {t('filterTables')}
          width='auto'
          onChange={e => setFilter(e.target.value)}
        />
        <Spacer />
        <Button
          leftIcon={<Icon as={FiLogOut} />}
          colorScheme="teal"
          size="sm"
          onClick={logout}
        >
          {t('logout')}
        </Button>
      </Flex>
    </Box>
  );
};

export default Header;
