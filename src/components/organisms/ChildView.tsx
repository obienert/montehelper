import React, { FC, useEffect, useState } from 'react';
import { Button, Checkbox, HStack, Text, VStack } from '@chakra-ui/react';
import useAPI from '../../utils/APIContextProvider';
import {useTranslation} from "react-i18next";
import DetailsFrame from "../atoms/DetailsFrame";
import FullName from "../atoms/FullName";
import { ChildWithDetails } from "../../types/models";

export type ChildViewProps = {
  childID: number
  close: () => void
  showAdult: (adultId: number) => void
}

const ChildView: FC<ChildViewProps> = ({childID, close, showAdult}) => {
  const { t } = useTranslation()

  const { getChild } = useAPI()
  const [child, setChild] = useState<ChildWithDetails>()
  const [error, setError] = useState(false)

  useEffect(() => {
    const fetchData = async () => {
      const child = await getChild(childID);

      setChild(child)
    }

    fetchData().catch(error => {
      console.log(error)
      setError(true)
    });
  }, [childID, getChild])

  return (
    <DetailsFrame close={close}>
      {error && <Text>Unexpected error occurred.</Text>}
      {(!child && !error) && <Text>No child with id {childID} exists.</Text>}
      {child &&
        <>
          <FullName person={child}/>
          <Text textStyle='description' >{t('birthdate')}</Text>
          <Text textStyle='content' >{t('birthDateFormat', { date: new Date(child.birth_date) })}</Text>
          <Text textStyle='description' >{t('birthplace')}</Text>
          <Text textStyle='content' >{child.birth_place}</Text>
          <Text textStyle='description' >{t('careTime')}</Text>
          <Text textStyle='content' >{t('careTimeDisplay', { careTime: child.care_time })}</Text>
          <Checkbox defaultChecked={child.kita} isReadOnly>
            {t('kita')}
          </Checkbox>
          {(Array.isArray(child.parents) && child.parents.length > 0) && <Text textStyle='description' >{t('parents')}</Text>}
          {child.parents?.map(parent => {
            if (parent) {
              return (<Button onClick={() => showAdult(parent.id)} variant={"person"}>
                {parent.first_name + " " + parent.last_name}
              </Button>)
            }
            return null
          })}
        </>
      }
    </DetailsFrame>
  );
};

export default ChildView;
