import {
  FormControl,
  FormLabel,
  IconButton,
  Input,
  InputGroup,
  InputRightElement,
  useDisclosure,
  useMergeRefs,
} from '@chakra-ui/react';
import * as React from 'react';
import { HiEye, HiEyeOff } from 'react-icons/hi';
import { useTranslation } from "react-i18next";

// TODO This should be typed, but I don't know how
export const PasswordField = React.forwardRef((props, ref) => {
  const { isOpen, onToggle } = useDisclosure();

  const inputRef = React.useRef();
  const mergeRef = useMergeRefs(inputRef, ref);
  const { t } = useTranslation();

  const onClickReveal = () => {
    onToggle();
    if (inputRef.current) {
      // @ts-ignore
      inputRef.current.focus({ preventScroll: true });
    }
  };

  return (
    <FormControl>
      <FormLabel htmlFor="password">{t('password')}</FormLabel>
      <InputGroup>
        <Input
          id="password"
          ref={mergeRef}
          name="password"
          type={isOpen ? 'text' : 'password'}
          autoComplete="current-password"
          required
          {...props}
        />
        <InputRightElement>
          <IconButton
            variant="link"
            aria-label={isOpen ? t('maskPassword') : t('revealPassword')}
            icon={isOpen ? <HiEyeOff /> : <HiEye />}
            onClick={onClickReveal}
          />
        </InputRightElement>
      </InputGroup>
    </FormControl>
  );
});
PasswordField.displayName = 'PasswordField';
