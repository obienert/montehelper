// @ts-nocheck
import React, {FC} from 'react';
import { Table, Tbody, Td, Th, Thead, Tr, chakra } from '@chakra-ui/react';
import { TriangleDownIcon, TriangleUpIcon } from '@chakra-ui/icons'
import {useTable, useSortBy, Column} from 'react-table'
import {Adult, Child} from "~/src/types/models";
import {useTranslation} from "react-i18next";

export type PersonTableProps = {
    showPerson: (personId: number) => void
    personID: number | undefined
    persons: Adult[]|Child[]
    columns: Column<Adult|Child>[]
}

const PersonTable: FC<PersonTableProps> = ({ persons, personID, showPerson, columns }) => {

    const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
        useTable<Adult|Child>({ columns, data: persons }, useSortBy)

    const { t } = useTranslation();

    return (
    <Table {...getTableProps()}>
      <Thead>
        {headerGroups.map(headerGroup =>
          <Tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column =>
              <Th
                {...column.getHeaderProps(column.getSortByToggleProps())}
                isNumeric={column.isNumeric}
              >
                {column.render('Header')}
                <chakra.span pl='4'>
                  {column.isSorted ? (
                    column.isSortedDesc ? (
                      <TriangleDownIcon aria-label={t('sortedDescending')} />
                    ) : (
                      <TriangleUpIcon aria-label={t('sortedAscending')} />
                    )
                  ) : null}
                </chakra.span>
              </Th>)}
          </Tr>
        )}
      </Thead>
      <Tbody {...getTableBodyProps()}>
        {rows.map((row) => {
          prepareRow(row)
          const highlight = personID === row.original.id
          return (
            <Tr
              bg={highlight ? "teal.100" : {}}
              fontWeight={highlight ? "semibold": {}}
              cursor={'pointer'}
              {...row.getRowProps()} onClick={() => showPerson(row.original.id)}>
              {row.cells.map((cell) => (
                <Td {...cell.getCellProps()} isNumeric={cell.column.isNumeric}>
                  {cell.render('Cell')}
                </Td>
              ))}
            </Tr>
          )
        })}
      </Tbody>
    </Table>
  );
};

export default PersonTable;
