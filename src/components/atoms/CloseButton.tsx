import React, {FC} from 'react';
import { Button } from '@chakra-ui/react'
import "../../styles/closeButton.css";

export type CloseProps = {
  close: () => void
}

const Close: FC<CloseProps> = ({close}) => {
  return (
    <Button
      position="absolute"
      top="2"
      right="2"
      size="5"
      onClick={close}
      className={"closeButton"}
    >
      X
    </Button>
  );
};

export default Close;
