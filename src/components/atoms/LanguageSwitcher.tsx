import React, { FC } from 'react';
import { useTranslation } from "react-i18next";
import { Select } from "@chakra-ui/react";
import '../../styles/languageSwitcher.css';

const LanguageSwitcher: FC = () => {
    const { t, i18n } = useTranslation()

    return (
        <Select
            width='auto'
            value={i18n.language}
            onChange={(e) => {
                i18n.changeLanguage(e.target.value).then()
            }}
        >
            <option value="en">{t('english')}</option>
            <option value="de">{t('german')}</option>
        </Select>
    );
};

export default LanguageSwitcher;
