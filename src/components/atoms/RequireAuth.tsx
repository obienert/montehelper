import React, { FC, ReactNode } from 'react';
import { Navigate } from 'react-router-dom';

import useAuth from "../../utils/APIHelper"

export type RequireAuthProps = {
  children: ReactNode
}

const RequireAuth: FC<RequireAuthProps> = ({ children }) => {
  const { isAuthenticated } = useAuth()
  console.log("isAuthenticated " + isAuthenticated)
  return isAuthenticated ? <>{children}</> : <Navigate to="/login" replace />
}

export default RequireAuth;
