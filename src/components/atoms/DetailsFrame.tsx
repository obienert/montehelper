import React, { FC, ReactNode } from 'react';
import { VStack } from "@chakra-ui/react";
import Close from "./CloseButton";

import '../../styles/detailsView.css';

export type DetailsFrameProps = {
  close: () => void
  children: ReactNode
}

const DetailsFrame: FC<DetailsFrameProps> = ({ close, children }) => {
  return (
    <VStack
      w={200}
      h="fit-content"
      p={5}
      alignItems="flex-start"
      shadow='md'
      borderWidth='1px'
      className={"detailsViewBox"}
    >
      <Close close={close}/>
      { children }
    </VStack>
  );
};

export default DetailsFrame;
