import React, { FC } from 'react';
import { HStack, Text, VStack } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import { Person } from "../../types/models";

export type FullNameProps = {
  person: Person
}

const FullName: FC<FullNameProps> = ({person}) => {
  const { t } = useTranslation()

  return (
    <HStack>
      <VStack alignItems="flex-start">
        <Text textStyle='description'>{t('firstName')}</Text>
        <Text textStyle='content'>{person.first_name}</Text>
      </VStack>
      <VStack alignItems="flex-start">
        <Text textStyle='description'>{t('lastName')}</Text>
        <Text textStyle='content'>{person.last_name}</Text>
      </VStack>
    </HStack>
  );
}

export default FullName;
