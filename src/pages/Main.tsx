import React, {useEffect, useState} from 'react';
import { Container, Flex } from '@chakra-ui/react';
import Header from '../components/organisms/Header';
import PersonList from '../components/organisms/PersonList';
import AdultView from '../components/organisms/AdultView';
import ChildView from '../components/organisms/ChildView';
import {useTranslation} from "react-i18next";

export enum PersonType {
  Adult,
  Child
}

const Main = () => {

  const [filter, setFilter] = useState("")
  const [adultID, setAdultID] = useState<number>()
  const [childID, setChildID] = useState<number>()
  const [personType, setPersonType] = useState<PersonType>(PersonType.Adult)

  const { t } = useTranslation()

  useEffect(() => { document.title = t('title') }, [t])

  const showAdult = (adultID: number) => {
    setPersonType(PersonType.Adult)
    setAdultID(adultID)
  }

  const showChild = (childID: number) => {
    setPersonType(PersonType.Child)
    setChildID(childID)
  }

  const closeAdult = () => {
    setAdultID(undefined)
  }

  const closeChild = () => {
    setChildID(undefined)
  }

  const getPersonID = (): number | undefined => {
    switch (personType) {
      case PersonType.Adult:
        return adultID
      case PersonType.Child:
        return childID
      default:
        return undefined
    }
  }

  const personID = getPersonID()

  return (
    <Container maxW="container.xl" p={0}>
      <Flex h="100vh" direction="column">
        <Header setFilter={setFilter}/>
        <Flex h="full" py={0}>
          <PersonList
            showAdult={showAdult}
            showChild={showChild}
            filter={filter}
            personType={personType}
            personID={personID}
            setPersonType={setPersonType}/>
          {personID && <>
            {personType === PersonType.Adult &&
              <AdultView adultID={personID} close={closeAdult} showAdult={showAdult} showChild={showChild}/>}
            {personType === PersonType.Child &&
              <ChildView childID={personID} close={closeChild} showAdult={showAdult}/>}
          </>}
        </Flex>
      </Flex>
    </Container>
  );
};

export default Main;
