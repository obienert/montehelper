import React, {FormEvent, useEffect, useState} from 'react';
import {
  Box,
  Button,
  Checkbox,
  Container,
  FormControl,
  FormLabel,
  Heading,
  HStack,
  Input,
  Stack,
} from '@chakra-ui/react';
import { PasswordField } from '../components/molecules/PasswordField';
import { Navigate, useNavigate } from 'react-router-dom';
import useAuth from '../utils/APIHelper';
import {useTranslation} from "react-i18next";
import LanguageSwitcher from "../components/atoms/LanguageSwitcher";

const Login = () => {
  const navigate = useNavigate()

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const { t } = useTranslation()

  const { isAuthenticated, login } = useAuth()

  useEffect(() => { document.title = t('titleLogin') }, [t])

  const handlePasswordChange = (event: FormEvent<HTMLInputElement>) => {
    setPassword(event.currentTarget.value)
  };

  const handleUserNameChange = (event: FormEvent<HTMLInputElement>) => {
    setUsername(event.currentTarget.value)
  };

  const handleLogin = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    login(username, password)
      .then(() => {
        navigate('/');
      })
  }

  return (
    isAuthenticated ? <Navigate to="/" replace /> :
    <form onSubmit={handleLogin}>
      <Container
        maxW="lg"
        py={{ base: '12', md: '24' }}
        px={{ base: '0', sm: '8' }}
      >
        <Stack spacing="8">
          <Stack spacing="6">
            <Stack spacing={{ base: '2', md: '3' }} textAlign="center">
              <Heading>{t('loginHeading')}</Heading>
            </Stack>
          </Stack>
          <Box
            py={{ base: '0', sm: '8' }}
            px={{ base: '4', sm: '10' }}
            bg={'transparent'}
            boxShadow={'none'}
            borderRadius={'none'}
          >
            <Stack spacing="6">
              <Stack spacing="5">
                <FormControl>
                  <FormLabel htmlFor="username">{t('username')}</FormLabel>
                  <Input
                    id="username"
                    type="text"
                    value={username}
                    onChange={handleUserNameChange}
                  />
                </FormControl>
                <PasswordField
                  /*
                  TODO see PasswordField.tsx
                  // @ts-ignore */
                  value={password}
                  onChange={handlePasswordChange}
                />
              </Stack>
              <HStack justify="space-between">
                <Checkbox defaultChecked>{t('rememberMe')}</Checkbox>
                <Button variant="link" colorScheme="blue" size="sm">
                  {t('forgotPassword')}
                </Button>
              </HStack>
              <Stack spacing="6">
                <Button type="submit" variant="solid">
                  {t('signin')}
                </Button>
                <LanguageSwitcher />
              </Stack>
            </Stack>
          </Box>
        </Stack>
      </Container>
    </form>
  );
};

export default Login;
