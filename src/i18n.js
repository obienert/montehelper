import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

import { format as formatDate, isDate } from "date-fns";
import { enGB, de } from "date-fns/locale";

const locales = { enGB, de };

i18n
    .use(Backend)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        fallbackLng: 'en',
        debug: true,

        interpolation: {
            escapeValue: false,
            format: (value, format, lng) => {
                if (isDate(value)) {
                    const locale = locales[lng]
                    return formatDate(value, format, { locale })
                }
            }
        }
    }).then();


export default i18n;
