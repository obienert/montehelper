import React, {createContext, FC, useContext, useEffect, useState} from 'react';
import { getAdultDetails, getAdults, getChildDetails, getChildren } from './APIHelper';
import { Adult, AdultWithDetails, Child, ChildWithDetails } from "../types/models";

export type APIContextProps = {
  adults: Adult[],
  loadingAdults: boolean,
  errorAdults: boolean,
  getAdult: (id: number | undefined) => Promise<AdultWithDetails | undefined>,
  children: Child[],
  loadingChildren: boolean,
  errorChildren: boolean,
  getChild: (id: number | undefined) => Promise<ChildWithDetails  | undefined>
}

// @ts-ignore
const APIContext = createContext<APIContextProps>();

export type APIContextProviderType = {
    children: React.ReactNode
}

export const APIContextProvider: FC<APIContextProviderType> = (props) => {
  const [adults, setAdults] = useState<Adult[]>([])
  const [loadingAdults, setLoadingAdults] = useState(true)
  const [errorAdults, setErrorAdults] = useState(false)

  const [children, setChildren] = useState<Child[]>([])
  const [loadingChildren, setLoadingChildren] = useState(true)
  const [errorChildren, setErrorChildren] = useState(false)

  const getAdult = async (id: number | undefined): Promise<AdultWithDetails | undefined> => {
    if (id === undefined)
      return undefined

    const data = await getAdultDetails(id)
      .then(res => {
        return res.data
      })
      .catch(error =>
        console.log(error)
      )
    return data
  }

  const getChild = async (id: number | undefined): Promise<ChildWithDetails | undefined> => {
    if (id === undefined)
      return undefined

    const data = await getChildDetails(id)
      .then(res => {
        return res.data
      })
      .catch(error =>
        console.log(error)
      )
    return data
  }

  useEffect(() => {
    getAdults()
      .then(res => {
        setAdults(res.data);
        setLoadingAdults(false)
      })
      .catch(error => {
        console.log(error)
        setErrorAdults(true);
      })
    getChildren()
      .then(res => {
        setChildren(res.data);
        setLoadingChildren(false)
      })
      .catch(error => {
        console.log(error)
        setErrorChildren(true);
      })
  }, [])

  return (
    <APIContext.Provider
      value={{
        adults,
        loadingAdults,
        errorAdults,
        getAdult,
        children,
        loadingChildren,
        errorChildren,
        getChild
      }}>
      {props.children}
    </APIContext.Provider>
  );
};

const useAPI = () => {
  const context = useContext(APIContext)
  if (context === undefined) {
    throw new Error("Context must be used within a Provider")
  }
  return context
}

export default useAPI;
