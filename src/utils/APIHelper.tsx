import React, {createContext, FC, useContext, useState} from 'react';
import axios, {AxiosResponse} from 'axios';

axios.defaults.headers.get['Content-Type'] = 'application/json'

// TODO specify the AxiosPromise type
export type AuthContextProps = {
  isAuthenticated: boolean,
  login: (username: string, password: string) => Promise<void>,
  logout: () => Promise<void>,
  getSession: () => Promise<void>
}

// @ts-ignore
const AuthContext = createContext<AuthContextProps>()

const useAuth = (): AuthContextProps => {
  const [csrf, setCsrf] = useState('')
  const [isAuthenticated, setIsAuthenticated] = useState(false)

  const login = (username: string, password: string): Promise<void> => {
    return axios('/api/login/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': csrf,
      },
      data: JSON.stringify({
        username: username,
        password: password,
      }),
    })
      .then(isResponseOk)
      .then(data => {
        console.log(data);
        setIsAuthenticated(true)
      })
      .catch(err => {
        console.log(err);
      });
  };

  const logout = (): Promise<void> => {
    return axios('/api/logout', {})
      .then(isResponseOk)
      .then(data => {
        console.log(data);
        setIsAuthenticated(false)
        getCSRF();
      })
      .catch(err => {
        console.log(err);
      });
  };

  const isResponseOk = (response: AxiosResponse) => {
    if (response.status >= 200 && response.status <= 299) {
      return response.data;
    } else {
      throw Error(response.statusText);
    }
  }

  const getCSRF = () => {
    return axios('/api/csrf/', {})
      .then(res => {
        let csrfToken = res.headers['x-csrftoken'];
        setCsrf(csrfToken)
        console.log(csrfToken);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const getSession = (): Promise<void> => {
    return axios('/api/session/', {})
      .then(res => {
        if (res.data.isAuthenticated) {
          setIsAuthenticated(true);
        }
      })
      .catch(error => {
        if (error.response && error.response.status === 403) {
          setIsAuthenticated(false);
          getCSRF();
        } else {
          // Something happened in setting up the request that triggered an Error
          console.log('Error', error.message);
        }
      });
  };

  return {
    isAuthenticated,
    login,
    logout,
    getSession
  }
};

export const getAdults = () => {
  return axios('api/adults', {})
}

export const getAdultDetails = (id: number) => {
  return axios(`api/adults/${id}`, {})
}

export const getChildren = () => {
  return axios('api/children', {})
}

export const getChildDetails = (id: number) => {
  return axios(`api/children/${id}`, {})
}

export type AuthProviderType = {
  children: React.ReactNode
}

export const AuthProvider: FC<AuthProviderType> = ({ children }) => {
  const auth = useAuth()
  return (
      <AuthContext.Provider value={auth}>
        {children}
      </AuthContext.Provider>)
}

const AuthConsumer = () => {
  return useContext(AuthContext)
}

export default AuthConsumer
