import {Adult, Child} from "../types/models";

export const filterList = <T extends Adult | Child>(list: Array<T>, filter: string) => {
  const result: T[] = []
  const lowerFilter = filter.toLowerCase()
  list.forEach(entry => {
    if (entry.last_name.toLowerCase().includes(lowerFilter)  ||
        entry.first_name.toLowerCase().includes(lowerFilter)) {
      result.push(entry)
    }
  })
  return result
}
